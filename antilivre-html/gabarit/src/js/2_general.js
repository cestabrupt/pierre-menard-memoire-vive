// Scripts
// Baffle - https://github.com/camwiegert/baffle
// With options

// Tercet
const btn = document.querySelector('.shuffle');
const tercet = document.querySelector('.tercet');
const menu = document.querySelector('.menu');
const titre = document.querySelector('.pagetitre');

Poeme();

btn.addEventListener('click', (e) => {
  e.preventDefault();
  Poeme();
});

menu.addEventListener('click', (e) => {
  e.preventDefault();
  titre.classList.toggle('show');
});

function Poeme() {
  const vers1 = texte.phrase1[Math.floor(Math.random() * texte.phrase1.length)];
  const vers2 = texte.phrase2[Math.floor(Math.random() * texte.phrase1.length)];
  const vers3 = texte.phrase3[Math.floor(Math.random() * texte.phrase1.length)];

  tercet.children[0].innerText = vers1;
  tercet.children[1].innerText = vers2;
  tercet.children[2].innerText = vers3;

  tercet.children[0].setAttribute("data-text", vers1);
  tercet.children[1].setAttribute("data-text", vers2);
  tercet.children[2].setAttribute("data-text", vers3);

  let b = baffle('.tercet .vhs')
   .reveal(500)
   .set({
      characters: 'memoirevive',
      speed: 50
});
}
