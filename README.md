# ~/ABRÜPT/PIERRE MÉNARD/MÉMOIRE VIVE/*

La [page de ce livre](https://abrupt.ch/pierre-menard/memoire-vive/) sur le réseau.

## Sur le livre

Ce texte en prose est une mémoire vive, une suite d’épiphanies. Sensation d’un récit qui se dessine fait de boucles, de spirales et de courbes. Non pas suites sans principes de construction mais entrelacements complexes. Couleurs, formes, collages, accidents s’inscrivent dans une dynamique du décloisonnement. Une tension entre le discontinu des fragments et le mouvement qui unifie l’ensemble, qui en détache des morceaux pour les travailler, les étudier sous tous les éclairages possibles, suspendre le cours d’une phrase, en retourner le cheminement, en déformer la logique, en fragmenter le sens, en désaccorder la syntaxe et avec elle toute linéarité, privilégiant les écarts de sens et d’images. Le texte est le montage de poèmes pris dans le réel, dont les fragments sont considérés comme surface de travail, espace à explorer.

## Sur l'auteur

Philippe Diaz est né en 1969. Il vit et travaille à Paris. Pierre Ménard est le pseudonyme qu’il a choisi en tant qu’écrivain. Rompu à l’exercice des allers-retours entre espaces tangibles et aires de substitution, praticien des écritures hybrides, très investi dans le champ de l’écriture et de la création numérique, il a publié de nombreux ouvrages aux formes variées.

[Liminaire](https://liminaire.fr), le site de Pierre Ménard.

## Sur la licence

Cet [antilivre](https://abrupt.ch/antilivre/) est disponible sous licence Creative Commons Attribution – Pas d’Utilisation Commerciale – Partage dans les Mêmes Conditions 4.0 International (CC-BY-NC-SA 4.0).

## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.ch) pour davantage d'informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.ch/partage/).
